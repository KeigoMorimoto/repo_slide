#version 120
//
// shader.frag
//
uniform mat4 InverseMat;
varying vec3 vNormal;


void main(void)
{

vec3 light = normalize(gl_LightSource[0].position.xyz);

	vec3 invLight = normalize(InverseMat*vec4(light,0.0)).xyz;
	float diffuse = clamp(dot(vNormal,invLight),0.0,1.0);
	//vec4  smpColor = texture2D(texture0, vec2(diffuse, 0.0));
	

	gl_FragColor = gl_Color*vec4(0.2,0.2,0.0,0.0);

	if(diffuse>0.2){
	gl_FragColor = gl_Color*vec4(0.8,0.8,0.0,0.0);
	}

	if(diffuse >0.8){
	gl_FragColor = gl_Color;	
	}
}