#version 120
//
// shader.vert
//

//attribute vec3 normal;

uniform mat4 MVP;
uniform bool edge;
varying vec3 vNormal;


void main(void)
{
vec4 pos = gl_Vertex;


// 法線と光源ベクトルとの内積(平行光源用)
vec3 normal = normalize(gl_NormalMatrix * gl_Normal);


if(edge){
	pos.xyz += gl_Normal.xyz*0.02;
	
}
gl_FrontColor = gl_Color;
vNormal = normal;
gl_Position = MVP * pos;
}