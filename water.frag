#version 120
//
// shader.frag
//
varying vec2 vuv;
uniform sampler2D texture;
uniform float time;

float wave(vec2 uv, vec2 emitter, float speed, float phase){
	float dst = distance(uv, emitter);
	return pow((0.5 + 0.5 * sin(dst * phase - time * speed)), 2.0);
}

void main(void){
	//vec2 tFrag = vec2(1.0 / 512.0);
	
	//vec3 color = texture2D(texture, vuv).rgb;
	
	
	vec2 position = vuv;

	float w = wave(position, vec2(0.3, 0.4), 2.0, 50.0);
	w += wave(position, vec2(0.6, 0.11), 2.0, 20.0);
	w += wave(position, vec2(0.9, 0.6), 10.0, 90.0);
	w += wave(position, vec2(0.1, 0.84), 10.0, 150.0);
	w += wave(position, vec2(0.32, 0.81), 10.0, 150.0);
	w += wave(position, vec2(0.16, 0.211), 10.0, 150.0);
	w += wave(position, vec2(0.39, 0.46), 10.0, 150.0);
	w += wave(position, vec2(0.51, 0.484), 10.0, 150.0);
	w += wave(position, vec2(0.732, 0.91), 10.0, 150.0);
	w += wave(position, vec2(0.5, 0.5), 2.0, 20.0);
	
	w *= 0.008;
	position += w;
	
	vec3 color =  texture2D(texture, position).rgb;

	color.r = texture2D(texture, position).r;
	color.g = texture2D(texture, position - vec2(0.002, 0)).g;
	color.b = texture2D(texture, position - vec2(0.004, 0)).b;

	gl_FragColor = vec4(color, 1.0).rgba;
}