﻿#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <windows.h>

#include "FrameBufferObject.h"
#include "OBJLoader.h"

#define _USE_MATH_DEFINES
#include <math.h>


#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

using glm::vec3;
using glm::vec4;
using glm::mat4;

using namespace std;

#define FBOWIDTH  512       // フレームバッファオブジェクトの幅
#define FBOHEIGHT 512       // フレームバッファオブジェクトの高さ

GLfloat lightColor[4] = { 1.0, 1.0, 1.0, 1.0 };
GLfloat lightPosition[4] = { 0.0, 3.0, 5.0, 1.0 };

//static GLuint fb;           // フレームバッファオブジェクト
//static GLuint cb;           // カラーバッファ用のテクスチャ


GLuint loadTexture(string filename,int width,int height)
{
	// テクスチャIDの生成
	GLuint texID;
	glGenTextures(1, &texID);

	// ファイルの読み込み
	std::ifstream fstr(filename, std::ios::binary);
	const size_t fileSize = static_cast<size_t>(fstr.seekg(0, fstr.end).tellg());
	fstr.seekg(0, fstr.beg);
	char* textureBuffer = new char[fileSize];
	fstr.read(textureBuffer, fileSize);

	// テクスチャをGPUに転送
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glBindTexture(GL_TEXTURE_2D, texID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, textureBuffer);

	// テクスチャの設定
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	// テクスチャのアンバインド
	delete[] textureBuffer;
	glBindTexture(GL_TEXTURE_2D, 0);

	return texID;
}

int readShaderSource(GLuint shaderObj, std::string fileName)
{
	//ファイルの読み込み
	std::ifstream ifs(fileName);
	if (!ifs)
	{
		std::cout << "error" << std::endl;
		return -1;
	}
	std::string source;
	std::string line;
	while (getline(ifs, line))
	{
		source += line + "\n";
	}
	// シェーダのソースプログラムをシェーダオブジェクトへ読み込む
	const GLchar *sourcePtr = (const GLchar *)source.c_str();
	GLint length = source.length();
	glShaderSource(shaderObj, 1, &sourcePtr, &length);
	return 0;
}

GLint makeShader(std::string vertexFileName, std::string fragmentFileName)
{
	// シェーダーオブジェクト作成
	GLuint vertShaderObj = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragShaderObj = glCreateShader(GL_FRAGMENT_SHADER);
	GLuint shader;
	// シェーダーコンパイルとリンクの結果用変数
	GLint compiled, linked;
	/* シェーダーのソースプログラムの読み込み */
	if (readShaderSource(vertShaderObj, vertexFileName)) return -1;
	if (readShaderSource(fragShaderObj, fragmentFileName)) return -1;
	/* バーテックスシェーダーのソースプログラムのコンパイル */
	glCompileShader(vertShaderObj);
	glGetShaderiv(vertShaderObj, GL_COMPILE_STATUS, &compiled);
	if (compiled == GL_FALSE)
	{
		fprintf(stderr, "Compile error in vertex shader.\n");
		return -1;
	}
	/* フラグメントシェーダーのソースプログラムのコンパイル */
	glCompileShader(fragShaderObj);
	glGetShaderiv(fragShaderObj, GL_COMPILE_STATUS, &compiled);
	if (compiled == GL_FALSE)
	{
		fprintf(stderr, "Compile error in fragment shader.\n");
		return -1;
	}
	/* プログラムオブジェクトの作成 */
	shader = glCreateProgram();
	/* シェーダーオブジェクトのシェーダープログラムへの登録 */
	glAttachShader(shader, vertShaderObj);
	glAttachShader(shader, fragShaderObj);
	/* シェーダーオブジェクトの削除 */
	glDeleteShader(vertShaderObj);
	glDeleteShader(fragShaderObj);
	/* シェーダープログラムのリンク */
	glLinkProgram(shader);
	glGetProgramiv(shader, GL_LINK_STATUS, &linked);
	if (linked == GL_FALSE)
	{
		fprintf(stderr, "Link error.\n");
		return -1;
	}
	return shader;
}

GLFWwindow* initGLFW(int width, int height)
{
	// GLFW初期化
	if (glfwInit() == GL_FALSE)
	{
		return nullptr;
	}
	// ウィンドウ生成
	GLFWwindow* window = glfwCreateWindow(width, height, "OpenGL Slide", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		return nullptr;
	}
	// バージョン2.1指定
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwMakeContextCurrent(window);
	glfwSwapInterval(0);
	// GLEW初期化
	if (glewInit() != GLEW_OK)
	{
		return nullptr;
	}
	return window;
}

void SetLighting()
{
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
	//glLightfv(GL_LIGHT0, GL_AMBIENT, lightColor);
}

int main()
{
	//Image.h側も変更する
	GLint width = 1980, height = 1080;
	GLFWwindow* window = initGLFW(width, height);

	// タイマーのセッティング
	double FPS = 60.0; // <--- 一秒間に更新する回数(30 か 60) 
	double currentTime, lastTime, elapsedTime, sleepTime;
	currentTime = lastTime = elapsedTime = sleepTime = 0.0;
	glfwSetTime(0.0); // <--- タイマーを初期化する

	

	GLuint texture_shader = makeShader("texture.vert", "texture.frag");
	GLuint water_shader = makeShader("texture.vert", "water.frag");
	GLuint game_shader = makeShader("texture.vert", "GameBoy.frag");
	GLuint game_shader2 = makeShader("texture.vert", "GameBoy2.frag");
	GLuint fire_shader = makeShader("texture.vert", "fire.frag");
	GLuint toon_shader = makeShader("toon.vert", "toon.frag");

	GLuint texID[6];

	texID[0] = loadTexture("slide.raw", 1024, 1024);
	texID[1] = loadTexture("game5.raw", 512, 512);
	texID[2] = loadTexture("water1.raw", 512, 512);
	texID[3] = loadTexture("TEST.raw" ,512, 512);
	texID[4] = loadTexture("3.raw", 512, 512);
	texID[5] = loadTexture("menu.raw", 1024, 1024);

	
	mat4 modelMat;
	mat4 mvpMat;
	mat4 inverseMat;

	// View行列を計算
	mat4 viewMat = glm::lookAt(
		vec3(0.0, 0.0, 2.0), // ワールド空間でのカメラの座標
		vec3(0.0, 0.0, 0.0), // 見ている位置の座標
		vec3(0.0, 1.0, 0.0)  // 上方向を示す。(0,1.0,0)に設定するとy軸が上になります
	);

	// Projection行列を計算
	mat4 projectionMat = glm::perspective(
		glm::radians(45.0f), // ズームの度合い(通常90〜30)
		(GLfloat)width / (GLfloat)height,		// アスペクト比
		0.1f,		// 近くのクリッピング平面
		1000.0f		// 遠くのクリッピング平面
	);

	// 頂点データ
	float vertex_position[] = {
		1.0f, 0.5625f,
		-1.0f, 0.5625f,
		-1.0f, -0.5625f,
		1.0f, -0.5625f
	};

	// 頂点データ
	float vertex_position2[] = {
		1.0f, 1.0f,
		-1.0f, 1.0f,
		-1.0f, -1.0f,
		1.0f, -1.0f
	};

	const GLfloat vertex_uv[] = {
		1, 0,
		0, 0,
		0, 1,
		1, 1,
	};
	/***texture.vert,texture.frag***/
	// 何番目のattribute変数か
	int positionLocation = glGetAttribLocation(texture_shader, "position");
	int uvLocation = glGetAttribLocation(texture_shader, "uv");
	int matrixID = glGetUniformLocation(texture_shader, "MVP");

	// attribute属性を有効にする
	glEnableVertexAttribArray(positionLocation);
	glEnableVertexAttribArray(uvLocation);

	/***texture.vert,game.frag***/
	int timeLocation1 = glGetUniformLocation(game_shader, "time");
	int useLocation = glGetUniformLocation(game_shader, "useShader");

	/***texture.vert,water.frag***/
	int timeLocation2 = glGetUniformLocation(water_shader, "time");

	/***texture.vert,fire.frag***/
	int timeLocation3 = glGetUniformLocation(fire_shader, "time");

	/***toon.vert,toon.frag***/
	GLuint matrixID2 = glGetUniformLocation(toon_shader, "MVP");
	GLuint inverseMatID = glGetUniformLocation(toon_shader, "InverseMat");
	int edgeLocation = glGetUniformLocation(toon_shader, "edge");
	//glEnableVertexAttribArray(edgeLocation);
	/***persona.vert,personashader.frag***/
	//int matrixID2 = glGetUniformLocation(persona_shader, "MVP");
	//int inverseMatID = glGetUniformLocation(persona_shader, "InverseMat");
	//int edgeLocation = glGetUniformLocation(persona_shader, "edge");
	/*************************************************************/
	// カラーバッファ用のテクスチャを用意する
	/*
	glGenTextures(1, &cb);
	glBindTexture(GL_TEXTURE_2D, cb);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, FBOWIDTH, FBOHEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);

	// フレームバッファオブジェクトを作成する
	glGenFramebuffersEXT(1, &fb);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fb);

	// フレームバッファオブジェクトにカラーバッファとしてテクスチャを結合する
	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, cb, 0);

	// フレームバッファオブジェクトの結合を解除する
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
	*/
	/***************************************************************/

	FramebufferObject fbo;
	fbo.Initialize(FBOWIDTH, FBOHEIGHT);

	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);//アルファの設定
	//glEnable(GL_BLEND);//アルファのブレンド有効

	//int useShaderLocation = glGetUniformLocation();

	/***ペルソナシェーダ***/
	//OBJMesh mesh;
	//　メッシュファイルの読み込み
	//mesh.Load("Mesh/dosei.obj");

	//　メッシュファイルの情報を表示
	//mesh.Information();

	// 現在のスライド番号
	int CurrentSlideNumber = 0;

	// スライド切り替え時間
	int count = 0;
	// アニメーション用時間
	double time = 0;


	// フレームループ
	while (glfwWindowShouldClose(window) == GL_FALSE)
	{
		currentTime = glfwGetTime();
		elapsedTime = currentTime - lastTime;
		sleepTime = 16.0 - elapsedTime;

		if (elapsedTime >= 1.0 / FPS) {

			//glEnable(GL_DEPTH_TEST);
			//glDepthFunc(GL_LESS);


			if (glfwGetKey(window, GLFW_KEY_RIGHT) != GLFW_RELEASE) {
				count++;
				if (count == 30) {
					CurrentSlideNumber++;
					cout << "CurrentSlideNumber" << CurrentSlideNumber << "\n";
				}
			}
			else if (glfwGetKey(window, GLFW_KEY_LEFT) != GLFW_RELEASE) {
				count++;
				if (count == 30) {
					CurrentSlideNumber--;
					cout << "CurrentSlideNumber" << CurrentSlideNumber << "\n";
				}
			}
			else if (glfwGetKey(window, GLFW_KEY_Q) != GLFW_RELEASE) {
				count++;
				if (count == 30) {
					CurrentSlideNumber--;
					cout << "SlideNumber" << "\n";
					cin >> CurrentSlideNumber;
				}
			}
			else {
				count = 0;
			}

			//modelMat = glm::rotate(glm::mat4(1.0f), 0.0f, vec3(0, 1, 0));
			//modelMat = glm::rotate(glm::mat4(modelMat), float(-80.0 * (M_PI / 180)), vec3(0, 1, 0));
			glClearColor(0.2f, 0.2f, 0.2f, 0.2f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			if (CurrentSlideNumber == 0) {
				

				glUseProgram(texture_shader);

				// attribute属性を登録
				glVertexAttribPointer(positionLocation, 2, GL_FLOAT, false, 0, vertex_position);
				glVertexAttribPointer(uvLocation, 2, GL_FLOAT, false, 0, vertex_uv);


				//modelMat = glm::translate(glm::mat4(90.0f), vec3(0, 0, 0));
				modelMat = glm::rotate(glm::mat4(1.0f), 0.0f, vec3(0, 1, 0));
				mvpMat = projectionMat * viewMat * modelMat;

				glUniformMatrix4fv(matrixID, 1, GL_FALSE, &mvpMat[0][0]);

				// モデルの描画
				glBindTexture(GL_TEXTURE_2D, texID[0]);
				glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

				glUseProgram(0);
			}

			if (CurrentSlideNumber == 1) {


				glUseProgram(texture_shader);

				// attribute属性を登録
				glVertexAttribPointer(positionLocation, 2, GL_FLOAT, false, 0, vertex_position);
				glVertexAttribPointer(uvLocation, 2, GL_FLOAT, false, 0, vertex_uv);


				//modelMat = glm::translate(glm::mat4(90.0f), vec3(0, 0, 0));
				modelMat = glm::rotate(glm::mat4(1.0f), 0.0f, vec3(0, 1, 0));
				mvpMat = projectionMat * viewMat * modelMat;

				glUniformMatrix4fv(matrixID, 1, GL_FALSE, &mvpMat[0][0]);

				// モデルの描画
				glBindTexture(GL_TEXTURE_2D, texID[5]);
				glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

				glUseProgram(0);
			}

			if (CurrentSlideNumber == 2) {


				glUseProgram(game_shader);

				// attribute属性を登録
				glVertexAttribPointer(positionLocation, 2, GL_FLOAT, false, 0, vertex_position);
				glVertexAttribPointer(uvLocation, 2, GL_FLOAT, false, 0, vertex_uv);

				glUniform1f(timeLocation1, time / 60);

				modelMat = glm::rotate(glm::mat4(1.0f), 0.0f, vec3(0, 1, 0));
				//modelMat = glm::translate(glm::mat4(1.0f), vec3(0, 0, 0));
				mvpMat = projectionMat * viewMat * modelMat;

				glUniformMatrix4fv(matrixID, 1, GL_FALSE, &mvpMat[0][0]);

				// モデルの描画
				glBindTexture(GL_TEXTURE_2D, texID[1]);
				glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

				glUseProgram(0);

				glUseProgram(texture_shader);

				// attribute属性を登録
				glVertexAttribPointer(positionLocation, 2, GL_FLOAT, false, 0, vertex_position);
				glVertexAttribPointer(uvLocation, 2, GL_FLOAT, false, 0, vertex_uv);

				glUniform1f(timeLocation1, time / 60);

				modelMat = glm::scale(glm::mat4(1.0f), vec3(0.1, 0.1, 1.0));
				modelMat = glm::rotate(modelMat, 0.0f, vec3(0, 1, 0));
				modelMat = glm::translate(modelMat, vec3(4.0, -2.2, 1));
				mvpMat = projectionMat * viewMat * modelMat;

				glUniformMatrix4fv(matrixID, 1, GL_FALSE, &mvpMat[0][0]);

				// モデルの描画
				glBindTexture(GL_TEXTURE_2D, texID[1]);
				glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

				glUseProgram(0);
			}

			if (CurrentSlideNumber == 3) {


				glUseProgram(game_shader2);

				// attribute属性を登録
				glVertexAttribPointer(positionLocation, 2, GL_FLOAT, false, 0, vertex_position);
				glVertexAttribPointer(uvLocation, 2, GL_FLOAT, false, 0, vertex_uv);

				glUniform1f(timeLocation1, time / 60);

				modelMat = glm::rotate(glm::mat4(1.0f), 0.0f, vec3(0, 1, 0));
				//modelMat = glm::translate(glm::mat4(1.0f), vec3(0, 0, 0));
				mvpMat = projectionMat * viewMat * modelMat;

				glUniformMatrix4fv(matrixID, 1, GL_FALSE, &mvpMat[0][0]);

				// モデルの描画
				glBindTexture(GL_TEXTURE_2D, texID[1]);
				glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

				glUseProgram(0);

				glUseProgram(texture_shader);

				// attribute属性を登録
				glVertexAttribPointer(positionLocation, 2, GL_FLOAT, false, 0, vertex_position);
				glVertexAttribPointer(uvLocation, 2, GL_FLOAT, false, 0, vertex_uv);

				glUniform1f(timeLocation1, time / 60);

				modelMat = glm::scale(glm::mat4(1.0f), vec3(0.1, 0.1, 1.0));
				modelMat = glm::rotate(modelMat, 0.0f, vec3(0, 1, 0));
				modelMat = glm::translate(modelMat, vec3(4.0, -2.2, 1));
				mvpMat = projectionMat * viewMat * modelMat;

				glUniformMatrix4fv(matrixID, 1, GL_FALSE, &mvpMat[0][0]);

				// モデルの描画
				glBindTexture(GL_TEXTURE_2D, texID[1]);
				glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

				glUseProgram(0);
			}


			if (CurrentSlideNumber == 4) {

				glUseProgram(water_shader);

				// attribute属性を登録
				glVertexAttribPointer(positionLocation, 2, GL_FLOAT, false, 0, vertex_position);
				glVertexAttribPointer(uvLocation, 2, GL_FLOAT, false, 0, vertex_uv);

				glUniform1f(timeLocation2, time / 60);

				modelMat = glm::rotate(glm::mat4(1.0f), 0.0f, vec3(0, 1, 0));
				//modelMat = glm::translate(glm::mat4(1.0f), vec3(0, 0, 0));
				mvpMat = projectionMat * viewMat * modelMat;

				glUniformMatrix4fv(matrixID, 1, GL_FALSE, &mvpMat[0][0]);

				// モデルの描画
				glBindTexture(GL_TEXTURE_2D, texID[2]);
				glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

				glUseProgram(0);

				glUseProgram(texture_shader);

				// attribute属性を登録
				glVertexAttribPointer(positionLocation, 2, GL_FLOAT, false, 0, vertex_position);
				glVertexAttribPointer(uvLocation, 2, GL_FLOAT, false, 0, vertex_uv);

				glUniform1f(timeLocation1, time / 60);

				modelMat = glm::scale(glm::mat4(1.0f), vec3(0.1, 0.1, 1.0));
				modelMat = glm::rotate(modelMat, 0.0f, vec3(0, 1, 0));
				modelMat = glm::translate(modelMat, vec3(4.0, -2.2, 1));
				mvpMat = projectionMat * viewMat * modelMat;

				glUniformMatrix4fv(matrixID, 1, GL_FALSE, &mvpMat[0][0]);

				// モデルの描画
				glBindTexture(GL_TEXTURE_2D, texID[2]);
				glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

				glUseProgram(0);

			}

			if (CurrentSlideNumber == 5) {

				glDisable(GL_CULL_FACE);

				// フレームバッファオブジェクトを結合する
				glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo.GetFramebufferID());

				// ビューポートの設定
				glViewport(0, 0, FBOWIDTH, FBOHEIGHT);

				glUseProgram(water_shader);

				// attribute属性を登録
				glVertexAttribPointer(positionLocation, 2, GL_FLOAT, false, 0, vertex_position);
				glVertexAttribPointer(uvLocation, 2, GL_FLOAT, false, 0, vertex_uv);

				glUniform1f(timeLocation2, time / 60);


				modelMat = glm::rotate(glm::mat4(1.0f), 0.0f, vec3(0, 1, 0));
				modelMat = glm::scale(modelMat, vec3(1.5, 1.5, 1.5));
				//modelMat = glm::translate(glm::mat4(1.0f), vec3(0, 0, 0));
				mvpMat = projectionMat * viewMat * modelMat;

				glUniformMatrix4fv(matrixID, 1, GL_FALSE, &mvpMat[0][0]);

				// モデルの描画
				glBindTexture(GL_TEXTURE_2D, texID[2]);
				glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

				glUseProgram(0);

				// フレームバッファオブジェクトを結合する
				glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

				glUseProgram(game_shader);

				// ビューポートはウィンドウのサイズに合わせる
				glViewport(0, 0, width, height);

				// attribute属性を登録
				glVertexAttribPointer(positionLocation, 2, GL_FLOAT, false, 0, vertex_position);
				glVertexAttribPointer(uvLocation, 2, GL_FLOAT, false, 0, vertex_uv);

				glUniform1f(timeLocation2, time / 60);

				//modelMat = glm::rotate(glm::mat4(1.0f), glm::radians(0.0f), vec3(0, 0, 1));
				modelMat = glm::rotate(modelMat, glm::radians(180.0f), vec3(1, 0, 0));
				//modelMat = glm::translate(glm::mat4(1.0f), vec3(0, 0, 0));
				mvpMat = projectionMat * viewMat * modelMat;

				glUniformMatrix4fv(matrixID, 1, GL_FALSE, &mvpMat[0][0]);

				// モデルの描画
				glBindTexture(GL_TEXTURE_2D, fbo.GetColorbufferID());
				glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

				glUseProgram(0);
			}
			if (CurrentSlideNumber == 6) {

				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);//アルファの設定
				glEnable(GL_BLEND);//アルファのブレンド有効

				glUseProgram(fire_shader);

				// attribute属性を登録
				glVertexAttribPointer(positionLocation, 2, GL_FLOAT, false, 0, vertex_position2);
				glVertexAttribPointer(uvLocation, 2, GL_FLOAT, false, 0, vertex_uv);

				glUniform1f(timeLocation3, time / 60);

				modelMat = glm::rotate(glm::mat4(1.0f), 0.0f, vec3(0, 1, 0));
				//modelMat = glm::translate(glm::mat4(1.0f), vec3(0, 0, 0));
				mvpMat = projectionMat * viewMat * modelMat;

				glUniformMatrix4fv(matrixID, 1, GL_FALSE, &mvpMat[0][0]);

				// モデルの描画
				glBindTexture(GL_TEXTURE_2D, texID[1]);
				glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

				glUseProgram(0);
			}
	
			time++;

			lastTime = glfwGetTime();

			// ダブルバッファのスワップ
			glfwSwapBuffers(window);
			glfwPollEvents();
		}
		Sleep(sleepTime);

	}
	// GLFWの終了処理
	glfwTerminate();
	return 0;
}